ST vers le 10 janvier 2019

ATTENTION !!!

Pour la compiation croisée, on ne peut pas utiliser ubuntu18 LTS car notamment les librairies SSL ne sont pas compatibles.
Il faut donc utiiser docker avec une version d'ubuntu plus vieille.
Docker ici n'est utilisé qu'avec des répertoires partagés !

0)
Attention les repertoires partages sont differents pour les différents cibles.
En effet si on prend un paquet qui est déjà compilé (avant l'installation de docker), certains éléments de :
    .../output/host/ utilisent le chemin absolu
C'est le cas notamment pour le quantiumV1.

Pour les machines remotev9 faire :
docker container run -it --rm -v /home/sebastien/Docker/volume/remotev9/:/home/sebastien/Docker/remotev9 ubuntu14_x

Pour les machines quantiumv2 faire :
docker container run -it --rm -v /home/sebastien/Docker/volume/quantiumv2/:/home/sebastien/Docker/quantiumv2 ubuntu14_x

Pour les machines quantiumv1 faire :
docker container run -it --rm -v /home/sebastien/Dropbox/UT_STORM_WIP/buildroot-QTFL/:/home/sebastien/Dropbox/UT_STORM_WIP/buildroot-QTFL/ ubuntu14_x


1) Créer la machine docker
-------------------------------
dans :
    /home/sebastien/Docker/dockerfiles/ubuntu14_xcompil
faire un :
    docker build -t ubuntu14_x .

ATTENTION : ne pas oublier le point à la fin de la ligne de commande !!

puis :
    docker container run --rm -it  -p 80:80 ubuntu14_x bash
pour vérifier que la machine est bien crée.

2) Les répertoires partagés :
-----------------------------
Les répertoires qui sont dans ~/Docker/volume ne doivent être utilisés qu'avec docker.
Pour monter et utiliser ces répertoires, il faut faire :

docker container run -it --rm -v /home/sebastien/Docker/volume/quantiumv2/:/home/sebastien/Docker/quantiumv2 ubuntu14_x

C'est important d'avoir le nom de la VM à la fin... 

puis aller dans (docker) dans :
     /home/sebastien/Docker/quantiumv2/
et lancer la compilation !!!
    make

Il n'était pas possible d'effectuer ces opérations sur ubuntu18LTS car des librairies (ssl notamment ne sont pas compatibles).


2) Créer la machine docker
-------------------------------
dans :
    /home/sebastien/Docker/dockerfiles/ubuntu14_xcompil
faire un :
    docker build -t ubuntu14_x .
puis 
    docker container run -it --rm -v /home/sebastien/Docker/volume/quantiumv2/:/home/sebastien/Docker/quantiumv2 ubuntu14_x

ou si on ne veut pas monter de volumes 
    docker container run --rm -it  -p 80:80 ubuntu14_x bash


3) 
Utilisation du docker :
-----------------------

une fois connecté faire un :
      cd /home/sebastien/Docker/quantiumv2/QuantiumV2Lots1_2_3/
puis make :


4)
Contenu du dockfile :
---------------------
Pour le futur et la possibilité de générer un make file, il faut effectuer les opérations suivantes :

EXPOSE 80
# - then it should use alpine package manager to install tini: 'apk add --update tini'
RUN apt-get update
# - then it should create directory /usr/src/app for app files with 'mkdir -p /usr/src/app'
WORKDIR /root
# - Node uses a "package manager", so it needs to copy in package.json file
RUN apt-get -y update
RUN apt-get -y upgrade
RUN apt-get -y install build-essential
RUN apt-get -y install wget
RUN apt-get -y install python
RUN apt-get -y install rsync
RUN apt-get -y install bc
RUN apt-get -y install unzip
RUN apt-get -y install git
RUN apt-get -y install libssl-dev
RUN apt-get -y install ncurses-dev
RUN apt-get -y install bison
RUN apt-get -y install flex
RUN apt-get -y install gettext
RUN apt-get -y install texinfo
RUN sudo useradd -p $(openssl passwd -1 quantaflow) sebastien
RUN locale-gen en_US.UTF-8
RUN locale-gen -no-purge -lang en_US.UTF8
RUN sudo useradd -p $(openssl passwd -1 quantaflow) sebastien
WORKDIR /home/sebastien/
